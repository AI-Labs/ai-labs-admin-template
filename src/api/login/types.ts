export interface UserLoginType {
  username: string
  password: string
}

export interface UserType {
  username: string
  password: string
  fullname: string
  nickname: string
  telephone: string
  avatar: string
  email: string
  gender: string
  method: string
  platform?: string
}
