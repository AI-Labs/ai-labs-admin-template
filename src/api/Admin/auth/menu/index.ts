import request from '@/axios'

export const getMenuTreeApi = (params: any): Promise<IResponse> => {
  return request.get({ url: '/admin/auth/menu/tree', params })
}

export const getMenuTreeOptionsApi = (): Promise<IResponse> => {
  return request.get({ url: '/admin/auth/menu/tree/options' })
}

export const saveMenuInfoApi = (data: any): Promise<IResponse> => {
  return request.post({ url: '/admin/auth/menu/save', data })
}

// #####################################################################
