import request from '@/axios'
import { DepartmentUserParams, DepartmentUserResponse } from './types'

export const getDepartmentTreeApi = (params: any) => {
  return request.get({ url: '/admin/auth/dept/tree', params })
}

export const saveDepartmentApi = (data: any) => {
  return request.post({ url: '/admin/auth/dept/save', data })
}

export const deleteDepartmentApi = (data: string[] | number[]) => {
  return request.delete({ url: '/admin/auth/dept/delete', data })
}

// #####################################################################

export const getUserByIdApi = (params: DepartmentUserParams) => {
  return request.get<DepartmentUserResponse>({ url: '/mock/department/users', params })
}

export const deleteUserByIdApi = (ids: string[] | number[]) => {
  return request.post({ url: '/mock/department/user/delete', data: { ids } })
}

export const saveUserApi = (data: any) => {
  return request.post({ url: '/mock/department/user/save', data })
}
