import request from '@/axios'

export const getCurrentUserInfo = (): Promise<IResponse> => {
  return request.get({ url: `/admin/auth/user/admin/current/info` })
}
