export default {
  common: {
    document: '项目文档',
    action: {
      title: '操作',
      add: '添加',
      edit: '编辑',
      detail: '详情',
      delete: '删除',
      logout: '退出系统',
      reload: '重新加载',
      closeTab: '关闭标签页',
      closeTheLeftTab: '关闭左侧标签页',
      closeTheRightTab: '关闭右侧标签页',
      closeOther: '关闭其它标签页',
      closeAll: '关闭全部标签页'
    },
    dialog: {
      add: '添加',
      edit: '编辑',
      detail: '详情',
      reminder: '温馨提示',
      logoutMessage: '是否退出本系统？'
    },
    error: {
      noPermission: `抱歉，您无权访问此页面。`,
      pageError: '抱歉，您访问的页面不存在。',
      networkError: '抱歉，服务器报告错误。',
      returnToHome: '返回首页'
    },
    info: {
      phone: '电话',
      email: '邮箱',
      desc: '描述'
    },
    lock: {
      back: '返回',
      backToLogin: '返回登录',
      entrySystem: '进入系统',
      lock: '锁定',
      lockPassword: '锁屏密码',
      lockScreen: '锁定屏幕',
      message: '锁屏密码错误',
      unlock: '点击解锁',
      placeholder: '请输入锁屏密码'
    },
    placeholder: {
      inputText: '请输入',
      selectText: '请选择'
    },
    setting: {
      projectSetting: '项目配置',
      theme: '主题',
      layout: '布局',
      systemTheme: '系统主题',
      menuTheme: '菜单主题',
      interfaceDisplay: '界面显示',
      breadcrumb: '面包屑',
      breadcrumbIcon: '面包屑图标',
      collapseMenu: '折叠菜单',
      hamburgerIcon: '折叠图标',
      screenfullIcon: '全屏图标',
      sizeIcon: '尺寸图标',
      localeIcon: '多语言图标',
      tagsView: '标签页',
      logo: 'Logo',
      greyMode: '灰色模式',
      fixedHeader: '固定头部',
      headerTheme: '头部主题',
      cutMenu: '切割菜单',
      copy: '拷贝',
      clearAndReset: '清除缓存并且重置',
      copySuccess: '拷贝成功',
      copyFailed: '拷贝失败',
      footer: '页脚',
      uniqueOpened: '菜单手风琴',
      tagsViewIcon: '标签页图标',
      dynamicRouter: '开启动态路由',
      serverDynamicRouter: '服务端动态路由',
      reExperienced: '请重新退出登录体验',
      fixedMenu: '固定菜单'
    },
    size: {
      default: '默认',
      large: '大',
      small: '小'
    },
    status: {
      hidden: '是否隐藏',
      disabled: '是否禁用',
      delete: '是否删除',
      order: '排序'
    },
    validator: {
      required: '该项为必填项'
    },
    workplace: {
      happyDay: '祝你开心每一天!',
      project: '项目',
      toDo: '待办',
      access: '项目访问',
      more: '更多',
      shortcutOperation: '快捷操作',
      operation: '操作',
      index: '指数',
      personal: '个人',
      team: '团队',
      quote: '引用',
      contribution: '贡献',
      hot: '热度',
      yield: '产量',
      dynamic: '动态',
      push: '推送',
      follow: '关注'
    }
  },
  login: {
    welcome: '欢迎使用本系统',
    message: '开箱即用的中后台管理系统',
    username: '用户名',
    password: '密码',
    login: '登录',
    register: '注册',
    checkPassword: '确认密码',
    otherLogin: '其它登录方式',
    remember: '记住我',
    hasUser: '已有账号？去登录',
    forgetPassword: '忘记密码',
    usernamePlaceholder: '请输入用户名',
    passwordPlaceholder: '请输入密码',
    code: '验证码',
    codePlaceholder: '请输入验证码',
    getCode: '获取验证码'
  },
  component: {
    button: {
      add: '添加',
      cancel: '取消',
      close: '关闭',
      ok: '确定',
      save: '保存'
    }
  },
  router: {
    personal: {
      center: '个人中心'
    }
  },
  admin: {
    menu: {
      title: '菜单名称',
      icon: '菜单图标',
      type: '菜单类型',
      path: '路由地址',
      component: '组件路径',
      perms: '权限标识',
      cache: '页面缓存'
    },
    role: {},
    dept: {
      parent: '上级部门',
      name: '部门名称',
      deptKey: '部门标识',
      owner: '负责人'
    },
    user: {}
  }
}
